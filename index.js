// Importar las dependencias
const express = require('express');
const app = express();
const { Server } = require("socket.io");

const cors = require('cors');

app.use(cors());
const http = require('http').createServer(app);

/* const io = require('socket.io')(http, {
  wssEngine: ["ws", "wss"],
  
  cors: {
      origin: "*",
      methods: ["GET", "POST"],
      transports: ['websocket', 'polling'],
      credentials: true
  },
  allowEIO3: true,
  pingTimeout: 60000,
}); */



const io = new Server(http, {
  wssEngine: ["ws", "wss"],
  transports: ["websocket", 'polling'],
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
    credentials: true
  },
  allowEIO3: true,
});





/*   io.on("connection", (socket) => {
    console.log("Alguien conecto");
  }); */
  



const { join } = require('node:path');


app.set('view engine', 'html');
// Definir una ruta para el API
app.get('/', (req, res) => {
  
  res.sendFile(join(__dirname, 'frm.html'));
});


// Configurar el WebSocket
io.on('connection', (socket) => {
    console.log('Cliente conectado');

    // Escuchar eventos del cliente
    socket.on('mensaje', (data) => {
      // console.log('Mensaje recibido:', data);
      let estadoLed = data.checked ? 'encendio' : 'apago';
      let led = data.control.substring(3);
      let response = {
        message: `Alguien ${estadoLed} el led ${led}`,
      }
      io.emit('mensaje', response);
    });

    // Manejar la desconexión del cliente
    socket.on('disconnect', () => {
      console.log('Cliente desconectado');
    });
});

// Iniciar el servidor
http.listen(process.env.PORT || 3000, () => {
    console.log('Servidor API iniciado en http://localhost:3000');
});